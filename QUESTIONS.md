# (B) Teoretická část

Popiš nám prosím, jak bys postupoval nebo jak by se tato tebou vytvořená aplikace musela upravit či
rozšířit v následujících situacích:

`Doplnich bych do aplikace cron. V závisloti, na nejmenší jednotce, jak často potřebujeme feedy kontrolovat, 
bych vytvořil command, který by se spouštěl. K pagi bych přidal čas poslední kontroly a dobu jak často se má kontrolovat.
`

1. Chtěli bysme, aby se konkrétní definované Facebook Pages stahovaly s vyšší frekvencí než
ostatní.

`Rozšiřit entity Page o přislušné nastavení, které se zohlední v Cronu`

2. Definovaný list Facebook Pages vzroste (třeba na tisíce) a je potřeba aplikaci škálovat a 
i s ohledem na SPOF (single point of failure) je potřeba ji provozovat simultánně na více 
různých serverech / virtuálních instancích.

`
   Fb nás pustí jen po 100 postech / request. 
   Tj. máme možnost si vytvořit třeba consumera, který tyto page bude spracovávat z rabbita
`

Dále prosím zodpověz tyto otázky:

3. Jak bys doporučil aplikaci monitorovat, aby sis byl jistý, že aplikace funguje nepřetržitě
správně? Jaké přidané další metriky / stavy by bylo dobré monitorovat?

`Přidal bych nový Controller(akci), který by kontroloval stav aplikace a db. 
Tuto akci bych pak v časových intervalech kontroloval. AWS ma na to hezkou monitorovací část aplikací.` 

`Přidla bych do aplikace monolog na zápis chyb z aplikace.`


4. Co by se mělo dít v případě, když se zjistí, že aplikace přestala fungovat? Jak se o takové
situaci dozvíme?

`1) Kontrola logů aplikace, zjištění co se stalo.`
`2) Na monolog lze navěsit odesálání emailů s errorem.`


5. Jak bys doporučil aplikaci testovat před nasazením do produkčního prostřední např. v
případě změn? Jak by měl vypadat deployment proces?

`Produkční a předproduční stage. Prévést build aplikace, spustit testy.
Na základě výsledků testů aplikaci nasadit či ne.`

6. Jaké věci bys doporučil v aplikaci loggovat, aby při případných chybách bylo snadné aplikaci
debuggovat a zjistit, kde se mohla vyskytnout příčina?

`
Ukládat 5** errory. (Monolog viz výše.)
`

7. Na jak velká data bude databáze, do které se data ukládají, stačit?

`
Pro větší množství dat v databázi začne být u relační databáze problém. Bylo by proto lepší využit jiný typ db. 
Další problém pro rychlost je proházenost záznamů v databázi pro posty. 
Využít by šlo například Cassandra pro ukládání a škalování databáze. 
`

8. Jaký a kde vidíš prostor pro optimalizaci výkonu této aplikace?

`
- použití jiné DB, DB cache pro výsledky (hlavně pro dotazy okolo Page)
- monžno uložit page i do APCu.
- použít RabbitMQ
- asynchroní zpracování dat, vlánka
- https://github.com/guzzle/promises
`