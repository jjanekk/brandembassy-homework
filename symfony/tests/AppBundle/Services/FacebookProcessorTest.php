<?php

declare(strict_types=1);

namespace Tests\AppBundle\Services;

use AppBundle\Entity\Page;
use AppBundle\Entity\Post;
use AppBundle\Service\FacebookProcessor;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * This is functional test...
 *
 * Class FacebookProcessorTest
 * @package Tests\AppBundle\Services
 */
class FacebookProcessorTest extends KernelTestCase
{

    /** @var  ContainerInterface */
    protected $container;


    /*
     * @todo
     * namockoval bych Facebook\Facebook
     * Otestoval bych počet volání v getPageItemFromUrl()
     *
     */


    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->container = $kernel->getContainer();
    }


    public function testGetPageItemFromUrl(): void
    {
        $fbProcessor = $this->container->get(FacebookProcessor::class);

        $page = $fbProcessor->getPageItemFromUrl('https://www.facebook.com/o2cz/');
        $this->assertInternalType('array', $page);

        $this->assertArrayHasKey('id', $page);
        $this->assertArrayHasKey('name', $page);
    }


    public function testLoadPostsFromPage(): void
    {
        $fbProcessor = $this->container->get(FacebookProcessor::class);
        $pageItem = $fbProcessor->getPageItemFromUrl('https://www.facebook.com/o2cz/');

        $page = new Page();
        $page->setPageId($pageItem['id']);

        $this->assertCount(10, $fbProcessor->loadPostsFromPage($page, 10));
        $this->assertCount(110, $fbProcessor->loadPostsFromPage($page, 110));
        $this->assertCount(100, $fbProcessor->loadPostsFromPage($page, 100));
    }


    public function testTransferItemIntoPost(): void
    {
        $fbProcessor = $this->container->get(FacebookProcessor::class);
        $pageItem = $fbProcessor->getPageItemFromUrl('https://www.facebook.com/o2cz/');

        $page = new Page();
        $page->setPageId($pageItem['id']);

        $dateTime = new \DateTime();

        $post = $fbProcessor->transferItemIntoPost([
            'id' => 'randomId',
            'message' => 'fb message',
            'created_time' => $dateTime
        ], $page);

        $this->assertInstanceOf(Post::class, $post);

        $this->assertEquals('randomId', $post->getMessageId());
        $this->assertEquals('fb message', $post->getMessage());
        $this->assertEquals($dateTime, $post->getCreatedTime());
    }
}
