<?php

declare(strict_types=1);

namespace AppBundle\Command;

use AppBundle\Entity\Page;
use AppBundle\Facade\AppFacade;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CronCommand
 * @package AppBundle\Command
 */
class CronPostCommand extends ContainerAwareCommand
{
    protected function configure(): void
    {
        $this
            ->setName('app:posts:import')
            ->setDescription('Import post from pages')
            ->addOption('page', null, InputOption::VALUE_OPTIONAL)
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, null, 200)
        ;
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \LogicException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Facebook\Exceptions\FacebookSDKException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \RuntimeException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('Importing posts...');

        $pageName = $input->getOption('page');
        $limit = (int)$input->getOption('limit');

        if ($pageName) {
            /** @var Page $page */
            $page = $this->getContainer()->get('doctrine.orm.default_entity_manager')
                ->getRepository(Page::class)
                ->findOneBy(['name' => $pageName]);

            if ($page === null) {
                throw new \RuntimeException("Page not found. ($pageName)");
            }

            $this->getContainer()->get(AppFacade::class)
                ->loadLastPostsFromPage($page, $limit);
        } else {
            $this->getContainer()->get(AppFacade::class)
                ->loadPostFromAllResources($limit);
        }

        $output->writeln('End of import');
    }
}
