<?php

declare(strict_types=1);

namespace AppBundle\Command;

use AppBundle\Entity\Page;
use AppBundle\Facade\AppFacade;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AddPageCommand
 * @package AppBundle\Command
 */
class AddPageCommand extends ContainerAwareCommand
{

    protected function configure(): void
    {
        $this
            ->setName('app:page:add')
            ->setDescription('Import post from pages')
            ->addArgument('uri', InputArgument::REQUIRED)
        ;
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $uri = $input->getArgument('uri');

        /** @var Page $page */
        $page = $this->getContainer()->get(AppFacade::class)
            ->createPageFromUri($uri);

        $output->writeln('Page created:');
        $output->writeln('Page name: ' . $page->getName());
        $output->writeln('Page id: ' . $page->getPageId());
        $output->writeln('Page uri: ' . $page->getPageUrl());
    }
}
