<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Page;
use AppBundle\Facade\AppFacade;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function indexAction(Request $request): Response
    {
        /** @var AppFacade $appFacade */
        $appFacade = $this->get(AppFacade::class);

        /** @var Page $page */
        $page = $appFacade->createPage();

        $pageForm =  $form = $this->createFormBuilder($page)
            ->add('pageUrl', UrlType::class)
            ->add('save', SubmitType::class, array('label' => 'Add Facebook page'))
            ->getForm();

        $pageForm->handleRequest($request);

        if ($pageForm->isSubmitted() && $pageForm->isValid()) {

            $data = $pageForm->getData();
            $appFacade->savePage($data);

            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/index.html.twig', [
            'page_form' => $pageForm->createView(),
            'pages'     => $appFacade->getPages(),
            'postCount' => $appFacade->getPostCountByPages(),
        ]);
    }


    /**
     * @Route("/page/{page}/remove", name="removePage")
     * @param Page $page
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function removePageAction(Page $page): Response
    {
        /** @var AppFacade $appFacade */
        $appFacade = $this->get(AppFacade::class);
        $appFacade->removePage($page);

        $this->get('session')
            ->getFlashBag()
            ->add('success', 'The page was removed.');

        return $this->redirectToRoute('homepage');
    }


    /**
     * @param Page $page
     * @param int $limit
     * @return JsonResponse
     *
     * @Route("/page/{page}/load-last-posts/{limit}", name="loadLastPostFromPage", requirements={"limit": "\d+"})
     */
    public function loadLastPostsFromPageAction(Page $page, int $limit = 100): JsonResponse
    {
        /** @var AppFacade $appFacade */
        $appFacade = $this->get(AppFacade::class);
        $response  = null;

        try {
            $appFacade->loadLastPostsFromPage($page, $limit);
            $response = new JsonResponse([
                'status' => 'ok',
                'id'     => $page->getId(),
                'count'  => $appFacade->getPostCountByPage($page),
            ], 200);
        } catch (\Exception $exception) { // ....
            $response = new JsonResponse('Error: ' . $exception->getMessage(), 500);
        }

        return $response;
    }


    /**
     * @param int $limit
     * @return JsonResponse
     *
     * @Route("/load-last-posts/{limit}", name="loadLastPosts", requirements={"limit": "\d+"})
     */
    public function loadLastPostFromAllPages(int $limit = 100): JsonResponse
    {
        /** @var AppFacade $appFacade */
        $appFacade = $this->get(AppFacade::class);

        try {
            $appFacade->loadPostFromAllResources($limit);

            $response = new JsonResponse(['status' => 'ok'], 200);
        } catch (\Exception $exception) { // ....
            $response = new JsonResponse('Error: ' . $exception->getMessage(), 500);
        }

        return $response;
    }
}
