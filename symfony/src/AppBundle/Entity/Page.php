<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Post
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(
 *     repositoryClass="\AppBundle\Entity\PageRepository"
 * )
 */
class Page
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $pageId;


    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $pageUrl;


    /**
     *
     * Is the length 100 sufficient?
     * https://stackoverflow.com/questions/8078939/what-is-the-maximum-length-of-a-facebook-name
     *
     * @var string
     * @ORM\Column(type="string", length=100)
     */
    protected $name;


    /**
     * @var double hour
     * @ORM\Column(type="float")
     */
    protected $frequency;


    /**
     * @var Post[]
     *
     * @ORM\OneToMany(
     *     targetEntity="\AppBundle\Entity\Post",
     *     mappedBy="page",
     *     cascade={"persist"}
     * )
     */
    protected $posts;


    /**
     * Page constructor.
     */
    public function __construct()
    {
        $this->pageUrl = '';
        $this->frequency = 1;

        $this->posts = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getPageId(): string
    {
        return $this->pageId;
    }


    /**
     * @param string $pageId
     */
    public function setPageId(string $pageId): void
    {
        $this->pageId = $pageId;
    }


    /**
     * @return string
     */
    public function getPageUrl(): string
    {
        return $this->pageUrl;
    }

    /**
     * @param string $pageUrl
     */
    public function setPageUrl(string $pageUrl): void
    {
        $this->pageUrl = $pageUrl;
    }


    /**
     * @return float
     */
    public function getFrequency(): float
    {
        return $this->frequency;
    }


    /**
     * @param float $frequency
     */
    public function setFrequency(float $frequency): void
    {
        $this->frequency = $frequency;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return Post[]
     */
    public function getPosts(): array
    {
        return $this->posts;
    }

    /**
     * @param Post[] $posts
     */
    public function setPosts(array $posts)
    {
        $this->posts = $posts;
    }


    /**
     * @param Post $post
     * @return Page
     */
    public function addPost(Post $post): Page
    {
        $this->posts->add($post);

        return $this;
    }
}
