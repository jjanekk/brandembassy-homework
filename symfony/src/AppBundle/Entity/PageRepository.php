<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class PageRepository
 * @package AppBundle\Entity
 */
class PageRepository extends EntityRepository
{

}
