<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Post
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(
 *     repositoryClass="\AppBundle\Entity\PostRepository"
 * )
 */
class Post
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;


    /**
     * Message_id - 32|length
     *
     * @var string
     * @ORM\Column(type="string", length=40)
     */
    protected $messageId;


    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $message;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdTime;


    /**
     * @var Page
     * @ORM\ManyToOne(
     *     targetEntity="\AppBundle\Entity\Page",
     *     inversedBy="posts",
     *     cascade={"persist"}
     * )
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $page;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getMessageId(): string
    {
        return $this->messageId;
    }


    /**
     * @param string $messageId
     */
    public function setMessageId(string $messageId): void
    {
        $this->messageId = $messageId;
    }


    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }


    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }


    /**
     * @return \DateTime
     */
    public function getCreatedTime(): \DateTime
    {
        return $this->createdTime;
    }


    /**
     * @param \DateTime $createdTime
     */
    public function setCreatedTime(\DateTime $createdTime): void
    {
        $this->createdTime = $createdTime;
    }


    /**
     * @param mixed $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }


    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }
}
