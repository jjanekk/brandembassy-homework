<?php

declare(strict_types=1);

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class PostRepository
 * @package AppBundle\Entity
 */
class PostRepository extends EntityRepository
{
    /**
     * Return post ids
     *
     * @param Page $page
     * @return null|string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastPostIdByPage(Page $page): ?string
    {
        $query = $this->getEntityManager()
            ->createQuery('
              SELECT post.messageId
              FROM  AppBundle:Post AS post
              WHERE post.page = :id
              ORDER BY post.createdTime DESC
            ')
            ->setMaxResults(1)
            ->setParameter('id', $page->getId())
        ;

        $id = null;
        if ($res = $query->getOneOrNullResult()) {
            $id = $res['messageId'];
        }

        return $id;
    }


    /**
     * @param Page $page
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function countByPage(Page $page): int
    {
        $query = $this->getEntityManager()
            ->createQuery('
              SELECT count(post) as postCount
              FROM  AppBundle:Post AS post
              WHERE post.page = :id
            ')
            ->setMaxResults(1)
            ->setParameter('id', $page->getId())
        ;

        return (int)$query->getSingleResult()['postCount'];
    }
}
