<?php

declare(strict_types=1);

namespace AppBundle\Service;

use AppBundle\Entity\Page;
use AppBundle\Entity\Post;

/**
 * Class PostReaderInterface
 * @package AppBundle\Service
 */
interface FacebookProcessorInterface
{

    /**
     * @param string $url
     * @return null|array
     */
    public function getPageItemFromUrl(string $url): ?array;


    /**
     * @param Page $page
     * @param int $limit
     * @return Post[]
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function loadPostsFromPage(Page $page, int $limit = 200): array;


    /**
     * @param array $item
     * @param Page $page
     * @return Post
     */
    public function transferItemIntoPost(array $item, Page $page): Post;

}
