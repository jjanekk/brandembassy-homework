<?php

declare(strict_types=1);

namespace AppBundle\Service;

use AppBundle\Entity\Page;
use AppBundle\Entity\Post;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\FacebookResponse;

/**
 * Class FacebookProcessor
 * @package AppBundle\Service
 */
class FacebookProcessor implements FacebookProcessorInterface
{
    /**
     * @var Facebook
     */
    private $facebook;


    /** @var array  */
    private $pages = [];


    /**
     * FacebookProcessor constructor.
     * @param Facebook $facebook
     */
    public function __construct(Facebook $facebook)
    {
        $this->facebook = $facebook;
    }


    /**
     * @param string $url
     * @return null|array
     */
    public function getPageItemFromUrl(string $url): ?array
    {
        $page = null;

        if (\array_key_exists($url, $this->pages)) {
            $page = $this->pages[$url];
        } else {

            $uriLocation = $this->parseResourcesFromUri($url);
            $response = $this->doRequest($uriLocation);

            if ($response) {
                $page = \json_decode($response->getBody(), true);
            }
        }

        return $page;
    }


    /**
     * @param Page $page
     * @param int $limit
     * @return Post[]
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function loadPostsFromPage(Page $page, int $limit = 200): array
    {
        // fb max limit = 100
        $posts = [];

        $maxLimitPerRequest = 100; // @todo const
        $requestLimit = 100;

        if ($limit >= $maxLimitPerRequest) {
            $itr = (int)($limit / $maxLimitPerRequest);

            for ($i = 0; $i < $itr; $i++) {
                $items = $this->getFeedItems($page->getPageId(), $requestLimit);

                /** @var \Facebook\GraphNodes\GraphNode $item */
                foreach ($items as $item) {
                    $posts[] = $this->transferItemIntoPost($item->asArray(), $page);
                }
            }
        }

        if (($rest = ($limit % $maxLimitPerRequest)) > 0) {
            $items = $this->getFeedItems($page->getPageId(), $rest);

            foreach ($items as $item) {
                $posts[] = $this->transferItemIntoPost($item->asArray(), $page);
            }
        }

        return $posts;
    }


    /**
     * @param array $item
     * @param Page $page
     * @return Post
     */
    public function transferItemIntoPost(array $item, Page $page): Post
    {
        $post = new Post();

        $message = '';
        if (isset($item['message'])) {
            $message = $item['message'];
        } elseif (isset($item['story'])) {
            $message = $item['story'];
        }

        $post->setCreatedTime($item['created_time']);
        $post->setMessage(\utf8_encode($message));
        $post->setMessageId($item['id']);
        $post->setPage($page);

        return $post;
    }


    /**
     *
     * Return null if request failed
     *
     * @param string $resource
     * @return FacebookResponse|null
     */
    private function doRequest(string $resource): ?FacebookResponse
    {
        $response = null;

        try {
            $response = $this->facebook->get($resource);
        } catch (FacebookSDKException $exception) {
            // @todo log $exception
            dump($exception);
        }

        return $response;
    }


    /**
     * @param string $id
     * @param int $requestLimit
     * @return array
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    private function getFeedItems(string $id, int $requestLimit): array
    {
        $items = [];

        $response = $this->doRequest($id. '/feed?limit=' . $requestLimit);
        if ($response) {
            $items = $response->getGraphEdge()->all();
        }

        return $items;
    }


    /**
     * Return page resource, eg. https://www.facebook.com/o2cz/ -> o2cz
     *
     * @param $uri
     * @return string
     */
    private function parseResourcesFromUri($uri): string
    {
        $parsedUrl = \parse_url($uri); // @todo sometime checked path exists...
        return  \str_replace('/', '', $parsedUrl['path']);
    }
}
