<?php

declare(strict_types=1);

namespace AppBundle\Facade;

use AppBundle\Entity\Page;
use AppBundle\Entity\PageRepository;
use AppBundle\Entity\Post;
use AppBundle\Entity\PostRepository;
use AppBundle\Service\FacebookProcessor;
use Doctrine\ORM\EntityManager;

/**
 * Class PostFacade
 * @package AppBundle\Facade
 */
class AppFacade
{
    /** @var  PageRepository */
    protected $pageRepository;

    /**
     * @var PostRepository
     */
    protected $postRepository;

    /** @var  FacebookProcessor */
    protected $facebookProcessor;


    /** @var  EntityManager */
    protected $entityManager;


    /**
     * PostFacade constructor.
     * @param PageRepository $pageRepository
     * @param PostRepository $postRepository
     * @param FacebookProcessor $facebookProcessor
     * @param EntityManager $entityManager
     */
    public function __construct(
        PageRepository $pageRepository,
        PostRepository $postRepository,
        FacebookProcessor $facebookProcessor,
        EntityManager $entityManager
    ) {
        $this->pageRepository = $pageRepository;
        $this->postRepository = $postRepository;
        $this->facebookProcessor = $facebookProcessor;
        $this->entityManager = $entityManager;
    }


    /**
     * Create new instance of Post
     *
     * @return Post
     */
    public function createPost(): Post
    {
        return new Post();
    }


    /**
     * Create new instance of Page
     *
     * @return Page
     */
    public function createPage(): Page
    {
        return new Page();
    }


    /**
     * @param Page $page
     * @throws \InvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function savePage(Page $page): void
    {
        /**
         * [
         *    'name' => 'Page Name',
         *    'id'   => 'page_id'
         * ]
         *
         * @var array
         */
        $facebookPageInfo = $this->facebookProcessor->getPageItemFromUrl($page->getPageUrl());

        /** @var Page|null $pageExistsTest */
        $pageExistsTest = $this->pageRepository
            ->findOneBy(['pageId' => $facebookPageInfo]);

        if ($pageExistsTest !== null) {
            throw new \InvalidArgumentException("This page({$pageExistsTest->getName()}) already exists.");
        }

        $page->setPageId($facebookPageInfo['id']);
        $page->setName($facebookPageInfo['name']);

        $this->entityManager->persist($page);
        $this->entityManager->flush();

        $this->loadLastPostsFromPage($page, 200);
    }


    /**
     * Remove page from DB
     *
     * @param Page $page
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removePage(Page $page): void
    {
        $this->entityManager->remove($page);
        $this->entityManager->flush();
    }


    /**
     * Returns list of pages
     *
     * @return array
     */
    public function getPages(): array
    {
        return $this->pageRepository->findAll();
    }


    /**
     * @param Page $page
     * @param int $limit
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Facebook\Exceptions\FacebookSDKException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadLastPostsFromPage(Page $page, int $limit = 100): void
    {
        $lastPostId = $this
            ->postRepository
            ->getLastPostIdByPage($page);

        $posts = $this
            ->facebookProcessor
            ->loadPostsFromPage($page, $limit);


        if ($lastPostId === null) { // read all
            $this->savePosts($posts);
        } else { // read until id
            $this->savePostsUntilId($posts, $lastPostId);
        }
    }


    /**
     * @param int $limit
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function loadPostFromAllResources(int $limit): void
    {
        foreach ($this->getPages() as $page) {
            $this->loadLastPostsFromPage($page, $limit);
        }
    }


    /**
     * Save posts
     *
     * @param array $posts
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function savePosts(array $posts): void
    {
        foreach ($posts as $post) {
            $this->entityManager->persist($post);
        }
        $this->entityManager->flush();
    }


    /**
     * @param array $posts
     * @param string $id
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function savePostsUntilId(array $posts, string $id): void
    {
        /** @var Post $post */
        foreach ($posts as $post) {
            if ($post->getMessageId() === $id) {
                break;
            }
            $this->entityManager->persist($post);
        }

        $this->entityManager->flush();
    }


    /**
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getPostCountByPages(): array
    {
        $postCount = [];

        /** @var Page $page */
        foreach ($this->getPages() as $page) {
            $postCount[$page->getId()] = $this->postRepository->countByPage($page);
        }

        return $postCount;
    }


    /**
     * @param Page $page
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getPostCountByPage(Page $page): int
    {
        return $this->postRepository->countByPage($page);
    }


    /**
     * @param string $uri
     * @return Page
     * @throws \InvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function createPageFromUri(string $uri): Page
    {
        $page = $this->createPage();
        $page->setPageUrl($uri);
        $this->savePage($page);

        return $page;
    }
}
