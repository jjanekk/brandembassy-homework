# Zadání:
## Vytvoření jednoduché aplikace

Vytvoř prosím PHP aplikaci, jejímž cílem bude pravidelně stahovat z Facebook API nové veřejné
posts z konkrétního listu definovaných Facebook Pages  (pro účel testu např. 3 Facebook Pages).

Tato data se budou ukládat do SQL tabulky (případně do NoSQL datbáze pokud je libo) tak, aby s nimi šlo následně pohodlně pracovat. V tabulce by se neměly objevit duplicitní záznamy. Zároveň vytvoř proces, který na začátku (první spuštění aplikace nebo přidání nové Facebook Page do listu) zajistí, aby se stáhlo posledních 200 posts. Definuj také do textové poznámky, jak by tvoje aplikace měla být následně spouštěna (v jaké periodě) nebo jiné náležitosti pro její provoz.

Při realizaci se snaž demonstrovat co největší využití objektového programování, dekompozici, defenzivitu, ošetření vstupů a potenciálních bezpečnostních chyb, unit tests, atd. Externí knihovny a libovolné frameworks mohou být využity. Další pomocné technologie rovněž nejsou limitovány. Zvolený coding standard je čistě na tobě.

Určitě však odevzdej kód, který bude za tebe v co nejlepším stavu, čitelnosti a učesanosti. Preferujeme anglická pojmenování kódu a anglické komentáře. Odevzdání může být v libovolném nezkompilovaném formátu nebo klidně jako zpřístupněný repozitář.



### PHP
---
Version: 7.1

### Spuštění aplikace:


#### Docker:
- `docker-compose up -d`
- `docker-compose exec php composer update`
- `docker-compose exec php php bin/console doctrine:schema:update --force`


#### Composer update:
`docker-compose exec php composer update`


#### Tests:
Basic unit tests.

`docker-compose exec php vendor/bin/phpunit `


#### Todo
- Create entrypoint.sh
- Add composer to entrypoint.sh
- Call Db update from entrypoint.sh
- Create link to phpunit


#### Commands:
- `bin/console app:page:add https://www.facebook.com/videacesky/`
- `bin/console app:posts:import`  //@todo put this to cron... 
- `bin/console app:posts:import --page="VideaČesky.cz"` VideaČesky.cz -> page name
- `bin/console app:posts:import --limit=10`